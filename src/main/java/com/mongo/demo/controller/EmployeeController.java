package com.mongo.demo.controller;

import com.mongo.demo.model.Employee;
import com.mongo.demo.repository.EmployeeRepository;
import com.mongo.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

//    @PostMapping("/create")
//    public Employee createEmployee(@RequestBody Employee createme){
//        employeeRepository.save(createme);
//        return createme;
//    }

//    @PostMapping("create")
//    public Employee createEmployee(@RequestBody Employee name,Employee position,Employee salary){
//        employeeService.createEmployee(name,position,salary);
//        return employeeService.findAll();
//    }

    @PostMapping("/create")
    public Employee createStudent(@RequestBody Employee creatme) {
        employeeRepository.save(creatme);
        return creatme;
    }

    @GetMapping("/aPath")
    public List<Employee> getEmployeeStuff(){
        employeeService.insertEmployee();
        return employeeRepository.findAll();
    }

    @GetMapping("/bPath")
    public Employee getRobert(){
        return employeeService.getRobert();
    }

    @GetMapping("/cPath")
    public Employee getSalar(){
        return employeeService.getSalar();
    }

    @GetMapping("/dPath")
    public List<Employee> getHighSalary(){
        return employeeService.getHighSalary();
    }

    @GetMapping("/ePath")
    public List<Employee> sortBySalary(){
        return employeeService.sortBySalary();
    }

    @GetMapping("/killPath")
    public List<Employee> remove(){
        return employeeService.remove();
    }

    @GetMapping("/nameList")
    public List<Employee> nameList(){
        return employeeService.returnName();
    }

    @GetMapping("/name")
    public List<Employee> nameA(){
        return employeeService.returnNameAC();
    }

    @GetMapping("/page")
    public List<Employee> page(){
        return employeeService.page();
    }


}
