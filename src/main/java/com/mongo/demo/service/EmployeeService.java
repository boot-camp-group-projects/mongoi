package com.mongo.demo.service;

import com.mongo.demo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    public EmployeeService(){}

    @Autowired
    private MongoTemplate mongoTemplate;

    public void insertEmployee(){
        Employee employees = new Employee();
        employees.setName("Craig");
        employees.setPosition("Boss");
        employees.setSalary(1000000.0);
        mongoTemplate.save(employees,"employee");
    }

    public Employee getRobert(){
        return mongoTemplate.findOne(Query.query(Criteria.where("name").is("Robert")),Employee.class);
    }


    public Employee getSalar(){
        return mongoTemplate.findOne(Query.query(Criteria.where("salary").gt(10000.0)),Employee.class);
    }

    public List<Employee> getHighSalary(){
        Query query = new Query();
        query.addCriteria(Criteria.where("salary").gt(100.0));
        List<Employee> employees = mongoTemplate.find(query,Employee.class);
        return employees;
    }

    public List<Employee> sortBySalary(){
        Query query = new Query();
        query.with(new Sort(Sort.DEFAULT_DIRECTION.DESC,"salary"));
        List<Employee> employees = mongoTemplate.find(query,Employee.class);
        return employees;
    }

    public List<Employee> remove(){
        Query query = new Query();
        query.addCriteria(Criteria.where("salary").gt(900000));
        List<Employee> employees = mongoTemplate.findAllAndRemove(query,Employee.class);
        return employees;
    }

    public List<Employee> returnName(){
        Query query = new Query();
        query.fields().exclude("id").exclude("salary").exclude("position");
        List<Employee> employees = mongoTemplate.find(query, Employee.class);
        return employees;
    }

    public List<Employee> returnNameAC(){
        Query query = new Query();
        query.addCriteria(Criteria.where("name").regex("B|A"));
        List<Employee> employees = mongoTemplate.find(query,Employee.class);
        return employees;
    }

    public List<Employee> page(){
        final Pageable pageableRequest = PageRequest.of(0, 4);
        Query query = new Query();
        query.with(pageableRequest);
        List<Employee> employees = mongoTemplate.find(query,Employee.class);
        return employees;
    }

}


