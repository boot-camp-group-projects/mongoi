package com.mongo.demo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
public class Employee {
    @Id
    private String id;

    private String name;

    private Double salary;

    private String position;

    public Employee(){}

    public Employee(String name, Double salary, String position){
        this.name = name;
        this.salary = salary;
        this.position = position;
    }
}
